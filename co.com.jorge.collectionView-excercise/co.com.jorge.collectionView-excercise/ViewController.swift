//
//  ViewController.swift
//  co.com.jorge.collectionView-excercise
//
//  Created by Jorge luis Menco Jaraba on 8/10/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var addNewItemButton: UIBarButtonItem!
    @IBOutlet weak var iconCollectionView: UICollectionView!
    
    @IBOutlet weak var deleteItemsButton: UIBarButtonItem!
    var collectionData = ["1🏆" , "2 🐸", "3 🍩", "4 😸", "5 🤡", "6 👾", "7 👻",
                          "8 🍖", "9 🎸", "10 🐯", "11 🐷", "12 🌋"]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupWidthToCell()
        setupLiveReload()
        addEditButton()
    }
    

    @IBAction private func addNewItem(_ sender: Any) {
        self.addItem()
    }
    
    @IBAction private func deleteItems(_ sender: Any) {
        if let selected = iconCollectionView.indexPathsForSelectedItems {
            let items = selected.map { $0.item }.sorted().reversed()
            for item in items {
                collectionData.remove(at: item)
            }
            iconCollectionView.deleteItems(at: selected)
        }
    }
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        addNewItemButton.isEnabled = !editing
        deleteItemsButton.isEnabled = editing
        iconCollectionView.allowsMultipleSelection = editing
        getSelectedItems(isEditing: editing)
    }
    
    private func setupCollectionView() {
        iconCollectionView.dataSource = self
        iconCollectionView.delegate = self
    }
    
    private func addEditButton() {
        navigationItem.leftBarButtonItem = editButtonItem
    }
    
    private func setupWidthToCell() {
        let cellWidth = (view.frame.size.width - 20) / 3
        let layout = iconCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellWidth)
    }
    
    private func setupLiveReload() {
        let liveReloaded = UIRefreshControl()
        liveReloaded.addTarget(self, action: #selector(self.refrestData), for: .valueChanged)
        iconCollectionView.refreshControl = liveReloaded
    }
    
    @objc private func refrestData() {
        self.addItem()
        endRefreshing()
    }
    
    private func endRefreshing() {
        iconCollectionView.refreshControl?.endRefreshing()
    }
    
    private func addItem() {
        let newItem = "\(collectionData.count + 1) 🐷"
        collectionData.append(newItem)
        let indexPathNewItem = IndexPath(row: collectionData.count - 1, section: 0)
        iconCollectionView.insertItems(at: [indexPathNewItem])
    }
    
    private func getSelectedItems(isEditing: Bool) {
        let indexPaths = iconCollectionView.indexPathsForVisibleItems
        let cellItems = indexPaths.map { (index) -> CustomCollectionViewCell in
            iconCollectionView.cellForItem(at: index) as! CustomCollectionViewCell
        }
        cellItems.forEach { (item) in
            item.isEditing = isEditing
        }
    }
}

extension ViewController: UICollectionViewDelegate {
    
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = iconCollectionView.dequeueReusableCell(withReuseIdentifier: "iconCollectionCell", for: indexPath) as! CustomCollectionViewCell
        cell.titleLabel.text = collectionData[indexPath.row]
        cell.isEditing = isEditing
        return cell
    }
}

